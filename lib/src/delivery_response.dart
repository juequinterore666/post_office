import 'dart:typed_data';

import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:post_office/post_office.dart';

import './package.dart';

part 'delivery_response.freezed.dart';

@freezed
abstract class RequestData<T> with _$RequestData {
  const factory RequestData(
      Package<T> originalPackage, PostOfficeConfig config) = _RequestData<T>;
}

@freezed
abstract class DeliveryResponse<R> with _$DeliveryResponse<R> {
  const factory DeliveryResponse(
      int statusCode, RequestData<Object> requestData,
      {R body,
      Uint8List bodyBytes,
      int contentLength,
      Map<String, String> headers,
      String reasonPhrase}) = _DeliveryResponse<R>;
}

@freezed
abstract class DeliveryError<T> with _$DeliveryError<T> {
  const factory DeliveryError.beforeDelivery(
          Exception exception, RequestData<Object> requestData) =
      _BeforeDelivery<T>;

  const factory DeliveryError.afterDelivery(
      DeliveryResponse<T> deliveryResponse) = _AfterDelivery<T>;
}
