import './package.dart';
import './post_office_base.dart';

abstract class DeliveryMan<T, R> {
  final Package<T> package;

  final PostOfficeConfig config;

  Future<R> deliver();

  DeliveryMan({this.package, this.config});
}
