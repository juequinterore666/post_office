import 'dart:convert';

import 'package:post_office/post_office.dart';

class EitherHttpDeliveryManJsonResponseParser
    extends EitherHttpDeliveryManResponseParser<Map<String, dynamic>> {
  @override
  Map<String, dynamic> parseResponseBody(String body) {
    return jsonDecode(body);
  }
}
