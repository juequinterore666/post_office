import 'package:freezed_annotation/freezed_annotation.dart';

part 'package.freezed.dart';

@freezed
abstract class Package<T> with _$Package<T> {
  static final Package NULL_PACKAGE = Package.get('');

  const factory Package.delete(String relativeAddress,
      {@Default({}) Map<String, String> headers,
      @Default({}) Map<String, String> addressParams,
      @Default({}) Map<String, String> addressQueries}) = _Delete<T>;

  const factory Package.get(String relativeAddress,
      {@Default({}) Map<String, String> headers,
      @Default({}) Map<String, String> addressParams,
      @Default({}) Map<String, String> addressQueries}) = _Get<T>;

  const factory Package.patch(
    String relativeAddress, {
    @Default({}) Map<String, String> headers,
    @Default({}) Map<String, String> addressParams,
    @Default({}) Map<String, String> addressQueries,
    T body,
  }) = _Patch<T>;

  const factory Package.post(
    String relativeAddress, {
    @Default({}) Map<String, String> headers,
    @Default({}) Map<String, String> addressParams,
    @Default({}) Map<String, String> addressQueries,
    T body,
  }) = _Post<T>;

  const factory Package.put(
    String relativeAddress, {
    @Default({}) Map<String, String> headers,
    @Default({}) Map<String, String> addressParams,
    @Default({}) Map<String, String> addressQueries,
    T body,
  }) = _Put<T>;
}
