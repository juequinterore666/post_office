// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'package.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

class _$PackageTearOff {
  const _$PackageTearOff();

// ignore: unused_element
  _Delete<T> delete<T>(String relativeAddress,
      {Map<String, String> headers = const {},
      Map<String, String> addressParams = const {},
      Map<String, String> addressQueries = const {}}) {
    return _Delete<T>(
      relativeAddress,
      headers: headers,
      addressParams: addressParams,
      addressQueries: addressQueries,
    );
  }

// ignore: unused_element
  _Get<T> get<T>(String relativeAddress,
      {Map<String, String> headers = const {},
      Map<String, String> addressParams = const {},
      Map<String, String> addressQueries = const {}}) {
    return _Get<T>(
      relativeAddress,
      headers: headers,
      addressParams: addressParams,
      addressQueries: addressQueries,
    );
  }

// ignore: unused_element
  _Patch<T> patch<T>(String relativeAddress,
      {Map<String, String> headers = const {},
      Map<String, String> addressParams = const {},
      Map<String, String> addressQueries = const {},
      T body}) {
    return _Patch<T>(
      relativeAddress,
      headers: headers,
      addressParams: addressParams,
      addressQueries: addressQueries,
      body: body,
    );
  }

// ignore: unused_element
  _Post<T> post<T>(String relativeAddress,
      {Map<String, String> headers = const {},
      Map<String, String> addressParams = const {},
      Map<String, String> addressQueries = const {},
      T body}) {
    return _Post<T>(
      relativeAddress,
      headers: headers,
      addressParams: addressParams,
      addressQueries: addressQueries,
      body: body,
    );
  }

// ignore: unused_element
  _Put<T> put<T>(String relativeAddress,
      {Map<String, String> headers = const {},
      Map<String, String> addressParams = const {},
      Map<String, String> addressQueries = const {},
      T body}) {
    return _Put<T>(
      relativeAddress,
      headers: headers,
      addressParams: addressParams,
      addressQueries: addressQueries,
      body: body,
    );
  }
}

// ignore: unused_element
const $Package = _$PackageTearOff();

mixin _$Package<T> {
  String get relativeAddress;
  Map<String, String> get headers;
  Map<String, String> get addressParams;
  Map<String, String> get addressQueries;

  @optionalTypeArgs
  Result when<Result extends Object>({
    @required
        Result delete(
            String relativeAddress,
            Map<String, String> headers,
            Map<String, String> addressParams,
            Map<String, String> addressQueries),
    @required
        Result get(
            String relativeAddress,
            Map<String, String> headers,
            Map<String, String> addressParams,
            Map<String, String> addressQueries),
    @required
        Result patch(
            String relativeAddress,
            Map<String, String> headers,
            Map<String, String> addressParams,
            Map<String, String> addressQueries,
            T body),
    @required
        Result post(
            String relativeAddress,
            Map<String, String> headers,
            Map<String, String> addressParams,
            Map<String, String> addressQueries,
            T body),
    @required
        Result put(
            String relativeAddress,
            Map<String, String> headers,
            Map<String, String> addressParams,
            Map<String, String> addressQueries,
            T body),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result delete(String relativeAddress, Map<String, String> headers,
        Map<String, String> addressParams, Map<String, String> addressQueries),
    Result get(String relativeAddress, Map<String, String> headers,
        Map<String, String> addressParams, Map<String, String> addressQueries),
    Result patch(
        String relativeAddress,
        Map<String, String> headers,
        Map<String, String> addressParams,
        Map<String, String> addressQueries,
        T body),
    Result post(
        String relativeAddress,
        Map<String, String> headers,
        Map<String, String> addressParams,
        Map<String, String> addressQueries,
        T body),
    Result put(
        String relativeAddress,
        Map<String, String> headers,
        Map<String, String> addressParams,
        Map<String, String> addressQueries,
        T body),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result delete(_Delete<T> value),
    @required Result get(_Get<T> value),
    @required Result patch(_Patch<T> value),
    @required Result post(_Post<T> value),
    @required Result put(_Put<T> value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result delete(_Delete<T> value),
    Result get(_Get<T> value),
    Result patch(_Patch<T> value),
    Result post(_Post<T> value),
    Result put(_Put<T> value),
    @required Result orElse(),
  });

  $PackageCopyWith<T, Package<T>> get copyWith;
}

abstract class $PackageCopyWith<T, $Res> {
  factory $PackageCopyWith(Package<T> value, $Res Function(Package<T>) then) =
      _$PackageCopyWithImpl<T, $Res>;
  $Res call(
      {String relativeAddress,
      Map<String, String> headers,
      Map<String, String> addressParams,
      Map<String, String> addressQueries});
}

class _$PackageCopyWithImpl<T, $Res> implements $PackageCopyWith<T, $Res> {
  _$PackageCopyWithImpl(this._value, this._then);

  final Package<T> _value;
  // ignore: unused_field
  final $Res Function(Package<T>) _then;

  @override
  $Res call({
    Object relativeAddress = freezed,
    Object headers = freezed,
    Object addressParams = freezed,
    Object addressQueries = freezed,
  }) {
    return _then(_value.copyWith(
      relativeAddress: relativeAddress == freezed
          ? _value.relativeAddress
          : relativeAddress as String,
      headers:
          headers == freezed ? _value.headers : headers as Map<String, String>,
      addressParams: addressParams == freezed
          ? _value.addressParams
          : addressParams as Map<String, String>,
      addressQueries: addressQueries == freezed
          ? _value.addressQueries
          : addressQueries as Map<String, String>,
    ));
  }
}

abstract class _$DeleteCopyWith<T, $Res> implements $PackageCopyWith<T, $Res> {
  factory _$DeleteCopyWith(_Delete<T> value, $Res Function(_Delete<T>) then) =
      __$DeleteCopyWithImpl<T, $Res>;
  @override
  $Res call(
      {String relativeAddress,
      Map<String, String> headers,
      Map<String, String> addressParams,
      Map<String, String> addressQueries});
}

class __$DeleteCopyWithImpl<T, $Res> extends _$PackageCopyWithImpl<T, $Res>
    implements _$DeleteCopyWith<T, $Res> {
  __$DeleteCopyWithImpl(_Delete<T> _value, $Res Function(_Delete<T>) _then)
      : super(_value, (v) => _then(v as _Delete<T>));

  @override
  _Delete<T> get _value => super._value as _Delete<T>;

  @override
  $Res call({
    Object relativeAddress = freezed,
    Object headers = freezed,
    Object addressParams = freezed,
    Object addressQueries = freezed,
  }) {
    return _then(_Delete<T>(
      relativeAddress == freezed
          ? _value.relativeAddress
          : relativeAddress as String,
      headers:
          headers == freezed ? _value.headers : headers as Map<String, String>,
      addressParams: addressParams == freezed
          ? _value.addressParams
          : addressParams as Map<String, String>,
      addressQueries: addressQueries == freezed
          ? _value.addressQueries
          : addressQueries as Map<String, String>,
    ));
  }
}

class _$_Delete<T> implements _Delete<T> {
  const _$_Delete(this.relativeAddress,
      {this.headers = const {},
      this.addressParams = const {},
      this.addressQueries = const {}})
      : assert(relativeAddress != null),
        assert(headers != null),
        assert(addressParams != null),
        assert(addressQueries != null);

  @override
  final String relativeAddress;
  @JsonKey(defaultValue: const {})
  @override
  final Map<String, String> headers;
  @JsonKey(defaultValue: const {})
  @override
  final Map<String, String> addressParams;
  @JsonKey(defaultValue: const {})
  @override
  final Map<String, String> addressQueries;

  @override
  String toString() {
    return 'Package<$T>.delete(relativeAddress: $relativeAddress, headers: $headers, addressParams: $addressParams, addressQueries: $addressQueries)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Delete<T> &&
            (identical(other.relativeAddress, relativeAddress) ||
                const DeepCollectionEquality()
                    .equals(other.relativeAddress, relativeAddress)) &&
            (identical(other.headers, headers) ||
                const DeepCollectionEquality()
                    .equals(other.headers, headers)) &&
            (identical(other.addressParams, addressParams) ||
                const DeepCollectionEquality()
                    .equals(other.addressParams, addressParams)) &&
            (identical(other.addressQueries, addressQueries) ||
                const DeepCollectionEquality()
                    .equals(other.addressQueries, addressQueries)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(relativeAddress) ^
      const DeepCollectionEquality().hash(headers) ^
      const DeepCollectionEquality().hash(addressParams) ^
      const DeepCollectionEquality().hash(addressQueries);

  @override
  _$DeleteCopyWith<T, _Delete<T>> get copyWith =>
      __$DeleteCopyWithImpl<T, _Delete<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required
        Result delete(
            String relativeAddress,
            Map<String, String> headers,
            Map<String, String> addressParams,
            Map<String, String> addressQueries),
    @required
        Result get(
            String relativeAddress,
            Map<String, String> headers,
            Map<String, String> addressParams,
            Map<String, String> addressQueries),
    @required
        Result patch(
            String relativeAddress,
            Map<String, String> headers,
            Map<String, String> addressParams,
            Map<String, String> addressQueries,
            T body),
    @required
        Result post(
            String relativeAddress,
            Map<String, String> headers,
            Map<String, String> addressParams,
            Map<String, String> addressQueries,
            T body),
    @required
        Result put(
            String relativeAddress,
            Map<String, String> headers,
            Map<String, String> addressParams,
            Map<String, String> addressQueries,
            T body),
  }) {
    assert(delete != null);
    assert(get != null);
    assert(patch != null);
    assert(post != null);
    assert(put != null);
    return delete(relativeAddress, headers, addressParams, addressQueries);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result delete(String relativeAddress, Map<String, String> headers,
        Map<String, String> addressParams, Map<String, String> addressQueries),
    Result get(String relativeAddress, Map<String, String> headers,
        Map<String, String> addressParams, Map<String, String> addressQueries),
    Result patch(
        String relativeAddress,
        Map<String, String> headers,
        Map<String, String> addressParams,
        Map<String, String> addressQueries,
        T body),
    Result post(
        String relativeAddress,
        Map<String, String> headers,
        Map<String, String> addressParams,
        Map<String, String> addressQueries,
        T body),
    Result put(
        String relativeAddress,
        Map<String, String> headers,
        Map<String, String> addressParams,
        Map<String, String> addressQueries,
        T body),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (delete != null) {
      return delete(relativeAddress, headers, addressParams, addressQueries);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result delete(_Delete<T> value),
    @required Result get(_Get<T> value),
    @required Result patch(_Patch<T> value),
    @required Result post(_Post<T> value),
    @required Result put(_Put<T> value),
  }) {
    assert(delete != null);
    assert(get != null);
    assert(patch != null);
    assert(post != null);
    assert(put != null);
    return delete(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result delete(_Delete<T> value),
    Result get(_Get<T> value),
    Result patch(_Patch<T> value),
    Result post(_Post<T> value),
    Result put(_Put<T> value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (delete != null) {
      return delete(this);
    }
    return orElse();
  }
}

abstract class _Delete<T> implements Package<T> {
  const factory _Delete(String relativeAddress,
      {Map<String, String> headers,
      Map<String, String> addressParams,
      Map<String, String> addressQueries}) = _$_Delete<T>;

  @override
  String get relativeAddress;
  @override
  Map<String, String> get headers;
  @override
  Map<String, String> get addressParams;
  @override
  Map<String, String> get addressQueries;
  @override
  _$DeleteCopyWith<T, _Delete<T>> get copyWith;
}

abstract class _$GetCopyWith<T, $Res> implements $PackageCopyWith<T, $Res> {
  factory _$GetCopyWith(_Get<T> value, $Res Function(_Get<T>) then) =
      __$GetCopyWithImpl<T, $Res>;
  @override
  $Res call(
      {String relativeAddress,
      Map<String, String> headers,
      Map<String, String> addressParams,
      Map<String, String> addressQueries});
}

class __$GetCopyWithImpl<T, $Res> extends _$PackageCopyWithImpl<T, $Res>
    implements _$GetCopyWith<T, $Res> {
  __$GetCopyWithImpl(_Get<T> _value, $Res Function(_Get<T>) _then)
      : super(_value, (v) => _then(v as _Get<T>));

  @override
  _Get<T> get _value => super._value as _Get<T>;

  @override
  $Res call({
    Object relativeAddress = freezed,
    Object headers = freezed,
    Object addressParams = freezed,
    Object addressQueries = freezed,
  }) {
    return _then(_Get<T>(
      relativeAddress == freezed
          ? _value.relativeAddress
          : relativeAddress as String,
      headers:
          headers == freezed ? _value.headers : headers as Map<String, String>,
      addressParams: addressParams == freezed
          ? _value.addressParams
          : addressParams as Map<String, String>,
      addressQueries: addressQueries == freezed
          ? _value.addressQueries
          : addressQueries as Map<String, String>,
    ));
  }
}

class _$_Get<T> implements _Get<T> {
  const _$_Get(this.relativeAddress,
      {this.headers = const {},
      this.addressParams = const {},
      this.addressQueries = const {}})
      : assert(relativeAddress != null),
        assert(headers != null),
        assert(addressParams != null),
        assert(addressQueries != null);

  @override
  final String relativeAddress;
  @JsonKey(defaultValue: const {})
  @override
  final Map<String, String> headers;
  @JsonKey(defaultValue: const {})
  @override
  final Map<String, String> addressParams;
  @JsonKey(defaultValue: const {})
  @override
  final Map<String, String> addressQueries;

  @override
  String toString() {
    return 'Package<$T>.get(relativeAddress: $relativeAddress, headers: $headers, addressParams: $addressParams, addressQueries: $addressQueries)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Get<T> &&
            (identical(other.relativeAddress, relativeAddress) ||
                const DeepCollectionEquality()
                    .equals(other.relativeAddress, relativeAddress)) &&
            (identical(other.headers, headers) ||
                const DeepCollectionEquality()
                    .equals(other.headers, headers)) &&
            (identical(other.addressParams, addressParams) ||
                const DeepCollectionEquality()
                    .equals(other.addressParams, addressParams)) &&
            (identical(other.addressQueries, addressQueries) ||
                const DeepCollectionEquality()
                    .equals(other.addressQueries, addressQueries)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(relativeAddress) ^
      const DeepCollectionEquality().hash(headers) ^
      const DeepCollectionEquality().hash(addressParams) ^
      const DeepCollectionEquality().hash(addressQueries);

  @override
  _$GetCopyWith<T, _Get<T>> get copyWith =>
      __$GetCopyWithImpl<T, _Get<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required
        Result delete(
            String relativeAddress,
            Map<String, String> headers,
            Map<String, String> addressParams,
            Map<String, String> addressQueries),
    @required
        Result get(
            String relativeAddress,
            Map<String, String> headers,
            Map<String, String> addressParams,
            Map<String, String> addressQueries),
    @required
        Result patch(
            String relativeAddress,
            Map<String, String> headers,
            Map<String, String> addressParams,
            Map<String, String> addressQueries,
            T body),
    @required
        Result post(
            String relativeAddress,
            Map<String, String> headers,
            Map<String, String> addressParams,
            Map<String, String> addressQueries,
            T body),
    @required
        Result put(
            String relativeAddress,
            Map<String, String> headers,
            Map<String, String> addressParams,
            Map<String, String> addressQueries,
            T body),
  }) {
    assert(delete != null);
    assert(get != null);
    assert(patch != null);
    assert(post != null);
    assert(put != null);
    return get(relativeAddress, headers, addressParams, addressQueries);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result delete(String relativeAddress, Map<String, String> headers,
        Map<String, String> addressParams, Map<String, String> addressQueries),
    Result get(String relativeAddress, Map<String, String> headers,
        Map<String, String> addressParams, Map<String, String> addressQueries),
    Result patch(
        String relativeAddress,
        Map<String, String> headers,
        Map<String, String> addressParams,
        Map<String, String> addressQueries,
        T body),
    Result post(
        String relativeAddress,
        Map<String, String> headers,
        Map<String, String> addressParams,
        Map<String, String> addressQueries,
        T body),
    Result put(
        String relativeAddress,
        Map<String, String> headers,
        Map<String, String> addressParams,
        Map<String, String> addressQueries,
        T body),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (get != null) {
      return get(relativeAddress, headers, addressParams, addressQueries);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result delete(_Delete<T> value),
    @required Result get(_Get<T> value),
    @required Result patch(_Patch<T> value),
    @required Result post(_Post<T> value),
    @required Result put(_Put<T> value),
  }) {
    assert(delete != null);
    assert(get != null);
    assert(patch != null);
    assert(post != null);
    assert(put != null);
    return get(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result delete(_Delete<T> value),
    Result get(_Get<T> value),
    Result patch(_Patch<T> value),
    Result post(_Post<T> value),
    Result put(_Put<T> value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (get != null) {
      return get(this);
    }
    return orElse();
  }
}

abstract class _Get<T> implements Package<T> {
  const factory _Get(String relativeAddress,
      {Map<String, String> headers,
      Map<String, String> addressParams,
      Map<String, String> addressQueries}) = _$_Get<T>;

  @override
  String get relativeAddress;
  @override
  Map<String, String> get headers;
  @override
  Map<String, String> get addressParams;
  @override
  Map<String, String> get addressQueries;
  @override
  _$GetCopyWith<T, _Get<T>> get copyWith;
}

abstract class _$PatchCopyWith<T, $Res> implements $PackageCopyWith<T, $Res> {
  factory _$PatchCopyWith(_Patch<T> value, $Res Function(_Patch<T>) then) =
      __$PatchCopyWithImpl<T, $Res>;
  @override
  $Res call(
      {String relativeAddress,
      Map<String, String> headers,
      Map<String, String> addressParams,
      Map<String, String> addressQueries,
      T body});
}

class __$PatchCopyWithImpl<T, $Res> extends _$PackageCopyWithImpl<T, $Res>
    implements _$PatchCopyWith<T, $Res> {
  __$PatchCopyWithImpl(_Patch<T> _value, $Res Function(_Patch<T>) _then)
      : super(_value, (v) => _then(v as _Patch<T>));

  @override
  _Patch<T> get _value => super._value as _Patch<T>;

  @override
  $Res call({
    Object relativeAddress = freezed,
    Object headers = freezed,
    Object addressParams = freezed,
    Object addressQueries = freezed,
    Object body = freezed,
  }) {
    return _then(_Patch<T>(
      relativeAddress == freezed
          ? _value.relativeAddress
          : relativeAddress as String,
      headers:
          headers == freezed ? _value.headers : headers as Map<String, String>,
      addressParams: addressParams == freezed
          ? _value.addressParams
          : addressParams as Map<String, String>,
      addressQueries: addressQueries == freezed
          ? _value.addressQueries
          : addressQueries as Map<String, String>,
      body: body == freezed ? _value.body : body as T,
    ));
  }
}

class _$_Patch<T> implements _Patch<T> {
  const _$_Patch(this.relativeAddress,
      {this.headers = const {},
      this.addressParams = const {},
      this.addressQueries = const {},
      this.body})
      : assert(relativeAddress != null),
        assert(headers != null),
        assert(addressParams != null),
        assert(addressQueries != null);

  @override
  final String relativeAddress;
  @JsonKey(defaultValue: const {})
  @override
  final Map<String, String> headers;
  @JsonKey(defaultValue: const {})
  @override
  final Map<String, String> addressParams;
  @JsonKey(defaultValue: const {})
  @override
  final Map<String, String> addressQueries;
  @override
  final T body;

  @override
  String toString() {
    return 'Package<$T>.patch(relativeAddress: $relativeAddress, headers: $headers, addressParams: $addressParams, addressQueries: $addressQueries, body: $body)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Patch<T> &&
            (identical(other.relativeAddress, relativeAddress) ||
                const DeepCollectionEquality()
                    .equals(other.relativeAddress, relativeAddress)) &&
            (identical(other.headers, headers) ||
                const DeepCollectionEquality()
                    .equals(other.headers, headers)) &&
            (identical(other.addressParams, addressParams) ||
                const DeepCollectionEquality()
                    .equals(other.addressParams, addressParams)) &&
            (identical(other.addressQueries, addressQueries) ||
                const DeepCollectionEquality()
                    .equals(other.addressQueries, addressQueries)) &&
            (identical(other.body, body) ||
                const DeepCollectionEquality().equals(other.body, body)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(relativeAddress) ^
      const DeepCollectionEquality().hash(headers) ^
      const DeepCollectionEquality().hash(addressParams) ^
      const DeepCollectionEquality().hash(addressQueries) ^
      const DeepCollectionEquality().hash(body);

  @override
  _$PatchCopyWith<T, _Patch<T>> get copyWith =>
      __$PatchCopyWithImpl<T, _Patch<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required
        Result delete(
            String relativeAddress,
            Map<String, String> headers,
            Map<String, String> addressParams,
            Map<String, String> addressQueries),
    @required
        Result get(
            String relativeAddress,
            Map<String, String> headers,
            Map<String, String> addressParams,
            Map<String, String> addressQueries),
    @required
        Result patch(
            String relativeAddress,
            Map<String, String> headers,
            Map<String, String> addressParams,
            Map<String, String> addressQueries,
            T body),
    @required
        Result post(
            String relativeAddress,
            Map<String, String> headers,
            Map<String, String> addressParams,
            Map<String, String> addressQueries,
            T body),
    @required
        Result put(
            String relativeAddress,
            Map<String, String> headers,
            Map<String, String> addressParams,
            Map<String, String> addressQueries,
            T body),
  }) {
    assert(delete != null);
    assert(get != null);
    assert(patch != null);
    assert(post != null);
    assert(put != null);
    return patch(relativeAddress, headers, addressParams, addressQueries, body);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result delete(String relativeAddress, Map<String, String> headers,
        Map<String, String> addressParams, Map<String, String> addressQueries),
    Result get(String relativeAddress, Map<String, String> headers,
        Map<String, String> addressParams, Map<String, String> addressQueries),
    Result patch(
        String relativeAddress,
        Map<String, String> headers,
        Map<String, String> addressParams,
        Map<String, String> addressQueries,
        T body),
    Result post(
        String relativeAddress,
        Map<String, String> headers,
        Map<String, String> addressParams,
        Map<String, String> addressQueries,
        T body),
    Result put(
        String relativeAddress,
        Map<String, String> headers,
        Map<String, String> addressParams,
        Map<String, String> addressQueries,
        T body),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (patch != null) {
      return patch(
          relativeAddress, headers, addressParams, addressQueries, body);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result delete(_Delete<T> value),
    @required Result get(_Get<T> value),
    @required Result patch(_Patch<T> value),
    @required Result post(_Post<T> value),
    @required Result put(_Put<T> value),
  }) {
    assert(delete != null);
    assert(get != null);
    assert(patch != null);
    assert(post != null);
    assert(put != null);
    return patch(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result delete(_Delete<T> value),
    Result get(_Get<T> value),
    Result patch(_Patch<T> value),
    Result post(_Post<T> value),
    Result put(_Put<T> value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (patch != null) {
      return patch(this);
    }
    return orElse();
  }
}

abstract class _Patch<T> implements Package<T> {
  const factory _Patch(String relativeAddress,
      {Map<String, String> headers,
      Map<String, String> addressParams,
      Map<String, String> addressQueries,
      T body}) = _$_Patch<T>;

  @override
  String get relativeAddress;
  @override
  Map<String, String> get headers;
  @override
  Map<String, String> get addressParams;
  @override
  Map<String, String> get addressQueries;
  T get body;
  @override
  _$PatchCopyWith<T, _Patch<T>> get copyWith;
}

abstract class _$PostCopyWith<T, $Res> implements $PackageCopyWith<T, $Res> {
  factory _$PostCopyWith(_Post<T> value, $Res Function(_Post<T>) then) =
      __$PostCopyWithImpl<T, $Res>;
  @override
  $Res call(
      {String relativeAddress,
      Map<String, String> headers,
      Map<String, String> addressParams,
      Map<String, String> addressQueries,
      T body});
}

class __$PostCopyWithImpl<T, $Res> extends _$PackageCopyWithImpl<T, $Res>
    implements _$PostCopyWith<T, $Res> {
  __$PostCopyWithImpl(_Post<T> _value, $Res Function(_Post<T>) _then)
      : super(_value, (v) => _then(v as _Post<T>));

  @override
  _Post<T> get _value => super._value as _Post<T>;

  @override
  $Res call({
    Object relativeAddress = freezed,
    Object headers = freezed,
    Object addressParams = freezed,
    Object addressQueries = freezed,
    Object body = freezed,
  }) {
    return _then(_Post<T>(
      relativeAddress == freezed
          ? _value.relativeAddress
          : relativeAddress as String,
      headers:
          headers == freezed ? _value.headers : headers as Map<String, String>,
      addressParams: addressParams == freezed
          ? _value.addressParams
          : addressParams as Map<String, String>,
      addressQueries: addressQueries == freezed
          ? _value.addressQueries
          : addressQueries as Map<String, String>,
      body: body == freezed ? _value.body : body as T,
    ));
  }
}

class _$_Post<T> implements _Post<T> {
  const _$_Post(this.relativeAddress,
      {this.headers = const {},
      this.addressParams = const {},
      this.addressQueries = const {},
      this.body})
      : assert(relativeAddress != null),
        assert(headers != null),
        assert(addressParams != null),
        assert(addressQueries != null);

  @override
  final String relativeAddress;
  @JsonKey(defaultValue: const {})
  @override
  final Map<String, String> headers;
  @JsonKey(defaultValue: const {})
  @override
  final Map<String, String> addressParams;
  @JsonKey(defaultValue: const {})
  @override
  final Map<String, String> addressQueries;
  @override
  final T body;

  @override
  String toString() {
    return 'Package<$T>.post(relativeAddress: $relativeAddress, headers: $headers, addressParams: $addressParams, addressQueries: $addressQueries, body: $body)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Post<T> &&
            (identical(other.relativeAddress, relativeAddress) ||
                const DeepCollectionEquality()
                    .equals(other.relativeAddress, relativeAddress)) &&
            (identical(other.headers, headers) ||
                const DeepCollectionEquality()
                    .equals(other.headers, headers)) &&
            (identical(other.addressParams, addressParams) ||
                const DeepCollectionEquality()
                    .equals(other.addressParams, addressParams)) &&
            (identical(other.addressQueries, addressQueries) ||
                const DeepCollectionEquality()
                    .equals(other.addressQueries, addressQueries)) &&
            (identical(other.body, body) ||
                const DeepCollectionEquality().equals(other.body, body)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(relativeAddress) ^
      const DeepCollectionEquality().hash(headers) ^
      const DeepCollectionEquality().hash(addressParams) ^
      const DeepCollectionEquality().hash(addressQueries) ^
      const DeepCollectionEquality().hash(body);

  @override
  _$PostCopyWith<T, _Post<T>> get copyWith =>
      __$PostCopyWithImpl<T, _Post<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required
        Result delete(
            String relativeAddress,
            Map<String, String> headers,
            Map<String, String> addressParams,
            Map<String, String> addressQueries),
    @required
        Result get(
            String relativeAddress,
            Map<String, String> headers,
            Map<String, String> addressParams,
            Map<String, String> addressQueries),
    @required
        Result patch(
            String relativeAddress,
            Map<String, String> headers,
            Map<String, String> addressParams,
            Map<String, String> addressQueries,
            T body),
    @required
        Result post(
            String relativeAddress,
            Map<String, String> headers,
            Map<String, String> addressParams,
            Map<String, String> addressQueries,
            T body),
    @required
        Result put(
            String relativeAddress,
            Map<String, String> headers,
            Map<String, String> addressParams,
            Map<String, String> addressQueries,
            T body),
  }) {
    assert(delete != null);
    assert(get != null);
    assert(patch != null);
    assert(post != null);
    assert(put != null);
    return post(relativeAddress, headers, addressParams, addressQueries, body);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result delete(String relativeAddress, Map<String, String> headers,
        Map<String, String> addressParams, Map<String, String> addressQueries),
    Result get(String relativeAddress, Map<String, String> headers,
        Map<String, String> addressParams, Map<String, String> addressQueries),
    Result patch(
        String relativeAddress,
        Map<String, String> headers,
        Map<String, String> addressParams,
        Map<String, String> addressQueries,
        T body),
    Result post(
        String relativeAddress,
        Map<String, String> headers,
        Map<String, String> addressParams,
        Map<String, String> addressQueries,
        T body),
    Result put(
        String relativeAddress,
        Map<String, String> headers,
        Map<String, String> addressParams,
        Map<String, String> addressQueries,
        T body),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (post != null) {
      return post(
          relativeAddress, headers, addressParams, addressQueries, body);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result delete(_Delete<T> value),
    @required Result get(_Get<T> value),
    @required Result patch(_Patch<T> value),
    @required Result post(_Post<T> value),
    @required Result put(_Put<T> value),
  }) {
    assert(delete != null);
    assert(get != null);
    assert(patch != null);
    assert(post != null);
    assert(put != null);
    return post(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result delete(_Delete<T> value),
    Result get(_Get<T> value),
    Result patch(_Patch<T> value),
    Result post(_Post<T> value),
    Result put(_Put<T> value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (post != null) {
      return post(this);
    }
    return orElse();
  }
}

abstract class _Post<T> implements Package<T> {
  const factory _Post(String relativeAddress,
      {Map<String, String> headers,
      Map<String, String> addressParams,
      Map<String, String> addressQueries,
      T body}) = _$_Post<T>;

  @override
  String get relativeAddress;
  @override
  Map<String, String> get headers;
  @override
  Map<String, String> get addressParams;
  @override
  Map<String, String> get addressQueries;
  T get body;
  @override
  _$PostCopyWith<T, _Post<T>> get copyWith;
}

abstract class _$PutCopyWith<T, $Res> implements $PackageCopyWith<T, $Res> {
  factory _$PutCopyWith(_Put<T> value, $Res Function(_Put<T>) then) =
      __$PutCopyWithImpl<T, $Res>;
  @override
  $Res call(
      {String relativeAddress,
      Map<String, String> headers,
      Map<String, String> addressParams,
      Map<String, String> addressQueries,
      T body});
}

class __$PutCopyWithImpl<T, $Res> extends _$PackageCopyWithImpl<T, $Res>
    implements _$PutCopyWith<T, $Res> {
  __$PutCopyWithImpl(_Put<T> _value, $Res Function(_Put<T>) _then)
      : super(_value, (v) => _then(v as _Put<T>));

  @override
  _Put<T> get _value => super._value as _Put<T>;

  @override
  $Res call({
    Object relativeAddress = freezed,
    Object headers = freezed,
    Object addressParams = freezed,
    Object addressQueries = freezed,
    Object body = freezed,
  }) {
    return _then(_Put<T>(
      relativeAddress == freezed
          ? _value.relativeAddress
          : relativeAddress as String,
      headers:
          headers == freezed ? _value.headers : headers as Map<String, String>,
      addressParams: addressParams == freezed
          ? _value.addressParams
          : addressParams as Map<String, String>,
      addressQueries: addressQueries == freezed
          ? _value.addressQueries
          : addressQueries as Map<String, String>,
      body: body == freezed ? _value.body : body as T,
    ));
  }
}

class _$_Put<T> implements _Put<T> {
  const _$_Put(this.relativeAddress,
      {this.headers = const {},
      this.addressParams = const {},
      this.addressQueries = const {},
      this.body})
      : assert(relativeAddress != null),
        assert(headers != null),
        assert(addressParams != null),
        assert(addressQueries != null);

  @override
  final String relativeAddress;
  @JsonKey(defaultValue: const {})
  @override
  final Map<String, String> headers;
  @JsonKey(defaultValue: const {})
  @override
  final Map<String, String> addressParams;
  @JsonKey(defaultValue: const {})
  @override
  final Map<String, String> addressQueries;
  @override
  final T body;

  @override
  String toString() {
    return 'Package<$T>.put(relativeAddress: $relativeAddress, headers: $headers, addressParams: $addressParams, addressQueries: $addressQueries, body: $body)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Put<T> &&
            (identical(other.relativeAddress, relativeAddress) ||
                const DeepCollectionEquality()
                    .equals(other.relativeAddress, relativeAddress)) &&
            (identical(other.headers, headers) ||
                const DeepCollectionEquality()
                    .equals(other.headers, headers)) &&
            (identical(other.addressParams, addressParams) ||
                const DeepCollectionEquality()
                    .equals(other.addressParams, addressParams)) &&
            (identical(other.addressQueries, addressQueries) ||
                const DeepCollectionEquality()
                    .equals(other.addressQueries, addressQueries)) &&
            (identical(other.body, body) ||
                const DeepCollectionEquality().equals(other.body, body)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(relativeAddress) ^
      const DeepCollectionEquality().hash(headers) ^
      const DeepCollectionEquality().hash(addressParams) ^
      const DeepCollectionEquality().hash(addressQueries) ^
      const DeepCollectionEquality().hash(body);

  @override
  _$PutCopyWith<T, _Put<T>> get copyWith =>
      __$PutCopyWithImpl<T, _Put<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required
        Result delete(
            String relativeAddress,
            Map<String, String> headers,
            Map<String, String> addressParams,
            Map<String, String> addressQueries),
    @required
        Result get(
            String relativeAddress,
            Map<String, String> headers,
            Map<String, String> addressParams,
            Map<String, String> addressQueries),
    @required
        Result patch(
            String relativeAddress,
            Map<String, String> headers,
            Map<String, String> addressParams,
            Map<String, String> addressQueries,
            T body),
    @required
        Result post(
            String relativeAddress,
            Map<String, String> headers,
            Map<String, String> addressParams,
            Map<String, String> addressQueries,
            T body),
    @required
        Result put(
            String relativeAddress,
            Map<String, String> headers,
            Map<String, String> addressParams,
            Map<String, String> addressQueries,
            T body),
  }) {
    assert(delete != null);
    assert(get != null);
    assert(patch != null);
    assert(post != null);
    assert(put != null);
    return put(relativeAddress, headers, addressParams, addressQueries, body);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result delete(String relativeAddress, Map<String, String> headers,
        Map<String, String> addressParams, Map<String, String> addressQueries),
    Result get(String relativeAddress, Map<String, String> headers,
        Map<String, String> addressParams, Map<String, String> addressQueries),
    Result patch(
        String relativeAddress,
        Map<String, String> headers,
        Map<String, String> addressParams,
        Map<String, String> addressQueries,
        T body),
    Result post(
        String relativeAddress,
        Map<String, String> headers,
        Map<String, String> addressParams,
        Map<String, String> addressQueries,
        T body),
    Result put(
        String relativeAddress,
        Map<String, String> headers,
        Map<String, String> addressParams,
        Map<String, String> addressQueries,
        T body),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (put != null) {
      return put(relativeAddress, headers, addressParams, addressQueries, body);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result delete(_Delete<T> value),
    @required Result get(_Get<T> value),
    @required Result patch(_Patch<T> value),
    @required Result post(_Post<T> value),
    @required Result put(_Put<T> value),
  }) {
    assert(delete != null);
    assert(get != null);
    assert(patch != null);
    assert(post != null);
    assert(put != null);
    return put(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result delete(_Delete<T> value),
    Result get(_Get<T> value),
    Result patch(_Patch<T> value),
    Result post(_Post<T> value),
    Result put(_Put<T> value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (put != null) {
      return put(this);
    }
    return orElse();
  }
}

abstract class _Put<T> implements Package<T> {
  const factory _Put(String relativeAddress,
      {Map<String, String> headers,
      Map<String, String> addressParams,
      Map<String, String> addressQueries,
      T body}) = _$_Put<T>;

  @override
  String get relativeAddress;
  @override
  Map<String, String> get headers;
  @override
  Map<String, String> get addressParams;
  @override
  Map<String, String> get addressQueries;
  T get body;
  @override
  _$PutCopyWith<T, _Put<T>> get copyWith;
}
