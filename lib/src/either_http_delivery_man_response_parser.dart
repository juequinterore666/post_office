import 'package:dartz/dartz.dart';
import 'package:http/src/response.dart';

import './delivery_response.dart';
import './http_delivery_man.dart';

abstract class EitherHttpDeliveryManResponseParser<R>
    implements
        HttpDeliveryManResponseParser<
            Either<DeliveryError<R>, DeliveryResponse<R>>> {
  @override
  Either<DeliveryError<R>, DeliveryResponse<R>> parseResponse(
      Response response, RequestData<Object> requestData) {
    final deliveryResponse = DeliveryResponse(response.statusCode, requestData,
        headers: response.headers,
        body: parseResponseBody(response.body),
        bodyBytes: response.bodyBytes,
        contentLength: response.contentLength,
        reasonPhrase: response.reasonPhrase);

    if (response.statusCode >= 400) {
      return Left(DeliveryError.afterDelivery(deliveryResponse));
    } else {
      return Right(deliveryResponse);
    }
  }

  @override
  Either<DeliveryError<R>, DeliveryResponse<R>> handleException(
      Exception exception, RequestData requestData) {
    return Left(DeliveryError.beforeDelivery(exception, requestData));
  }

  R parseResponseBody(String body);
}

class EitherHttpDeliveryManStringResponseParser
    extends EitherHttpDeliveryManResponseParser<String> {
  @override
  String parseResponseBody(String body) {
    return body;
  }
}
