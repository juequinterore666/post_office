import 'dart:collection';

import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:meta/meta.dart';

import './delivery_man.dart';
import './package.dart';

class PostOfficeConfig {
  final String cityAddress;
  final Map<String, String> _headers;

  PostOfficeConfig(
      {@required this.cityAddress, Map<String, String> headers = const {}})
      : _headers = Map.of(headers);

  void addHeader(String key, String value) {
    _headers[key] = value;
  }

  UnmodifiableMapView<String, String> get headers =>
      Map<String, String>.unmodifiable(_headers);

  @override
  String toString() {
    return 'PostOfficeConfig{cityAddress: $cityAddress, _headers: $_headers}';
  }

  @override
  bool operator ==(other) {
    return identical(this, other) ||
        (other is PostOfficeConfig &&
            cityAddress == other.cityAddress &&
            DeepCollectionEquality().equals(other.headers, _headers));
  }
}

class PostOffice<R> {
  final DeliveryMan<T, R> Function<T>(
      {Package<T> package, PostOfficeConfig config}) _deliveryManFactory;
  final PostOfficeConfig _defaultConfig;

  PostOffice(
      {@required
          cityAddress,
      defaultHeaders = const {},
      @required
          DeliveryMan<T, R> Function<T>(
                  {Package<T> package, PostOfficeConfig config})
              deliveryManFactory})
      : _defaultConfig =
            PostOfficeConfig(cityAddress: cityAddress, headers: defaultHeaders),
        _deliveryManFactory = deliveryManFactory;

  UnmodifiableMapView<String, String> get defaultHeaders =>
      _defaultConfig.headers;

  String get cityAddress => _defaultConfig.cityAddress;

  DeliveryMan<T, R> receive<T>(Package<T> package, {PostOfficeConfig config}) =>
      _deliveryManFactory(package: package, config: config ?? _defaultConfig);

  void addHeader(String key, String value) =>
      _defaultConfig.addHeader(key, value);

  Future<R> deliver<T>(Package<T> package, {PostOfficeConfig config}) {
    return receive(package, config: config).deliver();
  }
}
