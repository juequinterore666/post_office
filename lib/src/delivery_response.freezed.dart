// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'delivery_response.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

class _$RequestDataTearOff {
  const _$RequestDataTearOff();

// ignore: unused_element
  _RequestData<T> call<T>(Package<T> originalPackage, PostOfficeConfig config) {
    return _RequestData<T>(
      originalPackage,
      config,
    );
  }
}

// ignore: unused_element
const $RequestData = _$RequestDataTearOff();

mixin _$RequestData<T> {
  Package<T> get originalPackage;
  PostOfficeConfig get config;

  $RequestDataCopyWith<T, RequestData<T>> get copyWith;
}

abstract class $RequestDataCopyWith<T, $Res> {
  factory $RequestDataCopyWith(
          RequestData<T> value, $Res Function(RequestData<T>) then) =
      _$RequestDataCopyWithImpl<T, $Res>;
  $Res call({Package<T> originalPackage, PostOfficeConfig config});

  $PackageCopyWith<T, $Res> get originalPackage;
}

class _$RequestDataCopyWithImpl<T, $Res>
    implements $RequestDataCopyWith<T, $Res> {
  _$RequestDataCopyWithImpl(this._value, this._then);

  final RequestData<T> _value;
  // ignore: unused_field
  final $Res Function(RequestData<T>) _then;

  @override
  $Res call({
    Object originalPackage = freezed,
    Object config = freezed,
  }) {
    return _then(_value.copyWith(
      originalPackage: originalPackage == freezed
          ? _value.originalPackage
          : originalPackage as Package<T>,
      config: config == freezed ? _value.config : config as PostOfficeConfig,
    ));
  }

  @override
  $PackageCopyWith<T, $Res> get originalPackage {
    if (_value.originalPackage == null) {
      return null;
    }
    return $PackageCopyWith<T, $Res>(_value.originalPackage, (value) {
      return _then(_value.copyWith(originalPackage: value));
    });
  }
}

abstract class _$RequestDataCopyWith<T, $Res>
    implements $RequestDataCopyWith<T, $Res> {
  factory _$RequestDataCopyWith(
          _RequestData<T> value, $Res Function(_RequestData<T>) then) =
      __$RequestDataCopyWithImpl<T, $Res>;
  @override
  $Res call({Package<T> originalPackage, PostOfficeConfig config});

  @override
  $PackageCopyWith<T, $Res> get originalPackage;
}

class __$RequestDataCopyWithImpl<T, $Res>
    extends _$RequestDataCopyWithImpl<T, $Res>
    implements _$RequestDataCopyWith<T, $Res> {
  __$RequestDataCopyWithImpl(
      _RequestData<T> _value, $Res Function(_RequestData<T>) _then)
      : super(_value, (v) => _then(v as _RequestData<T>));

  @override
  _RequestData<T> get _value => super._value as _RequestData<T>;

  @override
  $Res call({
    Object originalPackage = freezed,
    Object config = freezed,
  }) {
    return _then(_RequestData<T>(
      originalPackage == freezed
          ? _value.originalPackage
          : originalPackage as Package<T>,
      config == freezed ? _value.config : config as PostOfficeConfig,
    ));
  }
}

class _$_RequestData<T> implements _RequestData<T> {
  const _$_RequestData(this.originalPackage, this.config)
      : assert(originalPackage != null),
        assert(config != null);

  @override
  final Package<T> originalPackage;
  @override
  final PostOfficeConfig config;

  @override
  String toString() {
    return 'RequestData<$T>(originalPackage: $originalPackage, config: $config)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _RequestData<T> &&
            (identical(other.originalPackage, originalPackage) ||
                const DeepCollectionEquality()
                    .equals(other.originalPackage, originalPackage)) &&
            (identical(other.config, config) ||
                const DeepCollectionEquality().equals(other.config, config)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(originalPackage) ^
      const DeepCollectionEquality().hash(config);

  @override
  _$RequestDataCopyWith<T, _RequestData<T>> get copyWith =>
      __$RequestDataCopyWithImpl<T, _RequestData<T>>(this, _$identity);
}

abstract class _RequestData<T> implements RequestData<T> {
  const factory _RequestData(
      Package<T> originalPackage, PostOfficeConfig config) = _$_RequestData<T>;

  @override
  Package<T> get originalPackage;
  @override
  PostOfficeConfig get config;
  @override
  _$RequestDataCopyWith<T, _RequestData<T>> get copyWith;
}

class _$DeliveryResponseTearOff {
  const _$DeliveryResponseTearOff();

// ignore: unused_element
  _DeliveryResponse<R> call<R>(int statusCode, RequestData<Object> requestData,
      {R body,
      Uint8List bodyBytes,
      int contentLength,
      Map<String, String> headers,
      String reasonPhrase}) {
    return _DeliveryResponse<R>(
      statusCode,
      requestData,
      body: body,
      bodyBytes: bodyBytes,
      contentLength: contentLength,
      headers: headers,
      reasonPhrase: reasonPhrase,
    );
  }
}

// ignore: unused_element
const $DeliveryResponse = _$DeliveryResponseTearOff();

mixin _$DeliveryResponse<R> {
  int get statusCode;
  RequestData<Object> get requestData;
  R get body;
  Uint8List get bodyBytes;
  int get contentLength;
  Map<String, String> get headers;
  String get reasonPhrase;

  $DeliveryResponseCopyWith<R, DeliveryResponse<R>> get copyWith;
}

abstract class $DeliveryResponseCopyWith<R, $Res> {
  factory $DeliveryResponseCopyWith(
          DeliveryResponse<R> value, $Res Function(DeliveryResponse<R>) then) =
      _$DeliveryResponseCopyWithImpl<R, $Res>;
  $Res call(
      {int statusCode,
      RequestData<Object> requestData,
      R body,
      Uint8List bodyBytes,
      int contentLength,
      Map<String, String> headers,
      String reasonPhrase});

  $RequestDataCopyWith<Object, $Res> get requestData;
}

class _$DeliveryResponseCopyWithImpl<R, $Res>
    implements $DeliveryResponseCopyWith<R, $Res> {
  _$DeliveryResponseCopyWithImpl(this._value, this._then);

  final DeliveryResponse<R> _value;
  // ignore: unused_field
  final $Res Function(DeliveryResponse<R>) _then;

  @override
  $Res call({
    Object statusCode = freezed,
    Object requestData = freezed,
    Object body = freezed,
    Object bodyBytes = freezed,
    Object contentLength = freezed,
    Object headers = freezed,
    Object reasonPhrase = freezed,
  }) {
    return _then(_value.copyWith(
      statusCode: statusCode == freezed ? _value.statusCode : statusCode as int,
      requestData: requestData == freezed
          ? _value.requestData
          : requestData as RequestData<Object>,
      body: body == freezed ? _value.body : body as R,
      bodyBytes:
          bodyBytes == freezed ? _value.bodyBytes : bodyBytes as Uint8List,
      contentLength: contentLength == freezed
          ? _value.contentLength
          : contentLength as int,
      headers:
          headers == freezed ? _value.headers : headers as Map<String, String>,
      reasonPhrase: reasonPhrase == freezed
          ? _value.reasonPhrase
          : reasonPhrase as String,
    ));
  }

  @override
  $RequestDataCopyWith<Object, $Res> get requestData {
    if (_value.requestData == null) {
      return null;
    }
    return $RequestDataCopyWith<Object, $Res>(_value.requestData, (value) {
      return _then(_value.copyWith(requestData: value));
    });
  }
}

abstract class _$DeliveryResponseCopyWith<R, $Res>
    implements $DeliveryResponseCopyWith<R, $Res> {
  factory _$DeliveryResponseCopyWith(_DeliveryResponse<R> value,
          $Res Function(_DeliveryResponse<R>) then) =
      __$DeliveryResponseCopyWithImpl<R, $Res>;
  @override
  $Res call(
      {int statusCode,
      RequestData<Object> requestData,
      R body,
      Uint8List bodyBytes,
      int contentLength,
      Map<String, String> headers,
      String reasonPhrase});

  @override
  $RequestDataCopyWith<Object, $Res> get requestData;
}

class __$DeliveryResponseCopyWithImpl<R, $Res>
    extends _$DeliveryResponseCopyWithImpl<R, $Res>
    implements _$DeliveryResponseCopyWith<R, $Res> {
  __$DeliveryResponseCopyWithImpl(
      _DeliveryResponse<R> _value, $Res Function(_DeliveryResponse<R>) _then)
      : super(_value, (v) => _then(v as _DeliveryResponse<R>));

  @override
  _DeliveryResponse<R> get _value => super._value as _DeliveryResponse<R>;

  @override
  $Res call({
    Object statusCode = freezed,
    Object requestData = freezed,
    Object body = freezed,
    Object bodyBytes = freezed,
    Object contentLength = freezed,
    Object headers = freezed,
    Object reasonPhrase = freezed,
  }) {
    return _then(_DeliveryResponse<R>(
      statusCode == freezed ? _value.statusCode : statusCode as int,
      requestData == freezed
          ? _value.requestData
          : requestData as RequestData<Object>,
      body: body == freezed ? _value.body : body as R,
      bodyBytes:
          bodyBytes == freezed ? _value.bodyBytes : bodyBytes as Uint8List,
      contentLength: contentLength == freezed
          ? _value.contentLength
          : contentLength as int,
      headers:
          headers == freezed ? _value.headers : headers as Map<String, String>,
      reasonPhrase: reasonPhrase == freezed
          ? _value.reasonPhrase
          : reasonPhrase as String,
    ));
  }
}

class _$_DeliveryResponse<R> implements _DeliveryResponse<R> {
  const _$_DeliveryResponse(this.statusCode, this.requestData,
      {this.body,
      this.bodyBytes,
      this.contentLength,
      this.headers,
      this.reasonPhrase})
      : assert(statusCode != null),
        assert(requestData != null);

  @override
  final int statusCode;
  @override
  final RequestData<Object> requestData;
  @override
  final R body;
  @override
  final Uint8List bodyBytes;
  @override
  final int contentLength;
  @override
  final Map<String, String> headers;
  @override
  final String reasonPhrase;

  @override
  String toString() {
    return 'DeliveryResponse<$R>(statusCode: $statusCode, requestData: $requestData, body: $body, bodyBytes: $bodyBytes, contentLength: $contentLength, headers: $headers, reasonPhrase: $reasonPhrase)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _DeliveryResponse<R> &&
            (identical(other.statusCode, statusCode) ||
                const DeepCollectionEquality()
                    .equals(other.statusCode, statusCode)) &&
            (identical(other.requestData, requestData) ||
                const DeepCollectionEquality()
                    .equals(other.requestData, requestData)) &&
            (identical(other.body, body) ||
                const DeepCollectionEquality().equals(other.body, body)) &&
            (identical(other.bodyBytes, bodyBytes) ||
                const DeepCollectionEquality()
                    .equals(other.bodyBytes, bodyBytes)) &&
            (identical(other.contentLength, contentLength) ||
                const DeepCollectionEquality()
                    .equals(other.contentLength, contentLength)) &&
            (identical(other.headers, headers) ||
                const DeepCollectionEquality()
                    .equals(other.headers, headers)) &&
            (identical(other.reasonPhrase, reasonPhrase) ||
                const DeepCollectionEquality()
                    .equals(other.reasonPhrase, reasonPhrase)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(statusCode) ^
      const DeepCollectionEquality().hash(requestData) ^
      const DeepCollectionEquality().hash(body) ^
      const DeepCollectionEquality().hash(bodyBytes) ^
      const DeepCollectionEquality().hash(contentLength) ^
      const DeepCollectionEquality().hash(headers) ^
      const DeepCollectionEquality().hash(reasonPhrase);

  @override
  _$DeliveryResponseCopyWith<R, _DeliveryResponse<R>> get copyWith =>
      __$DeliveryResponseCopyWithImpl<R, _DeliveryResponse<R>>(
          this, _$identity);
}

abstract class _DeliveryResponse<R> implements DeliveryResponse<R> {
  const factory _DeliveryResponse(
      int statusCode, RequestData<Object> requestData,
      {R body,
      Uint8List bodyBytes,
      int contentLength,
      Map<String, String> headers,
      String reasonPhrase}) = _$_DeliveryResponse<R>;

  @override
  int get statusCode;
  @override
  RequestData<Object> get requestData;
  @override
  R get body;
  @override
  Uint8List get bodyBytes;
  @override
  int get contentLength;
  @override
  Map<String, String> get headers;
  @override
  String get reasonPhrase;
  @override
  _$DeliveryResponseCopyWith<R, _DeliveryResponse<R>> get copyWith;
}

class _$DeliveryErrorTearOff {
  const _$DeliveryErrorTearOff();

// ignore: unused_element
  _BeforeDelivery<T> beforeDelivery<T>(
      Exception exception, RequestData<Object> requestData) {
    return _BeforeDelivery<T>(
      exception,
      requestData,
    );
  }

// ignore: unused_element
  _AfterDelivery<T> afterDelivery<T>(DeliveryResponse<T> deliveryResponse) {
    return _AfterDelivery<T>(
      deliveryResponse,
    );
  }
}

// ignore: unused_element
const $DeliveryError = _$DeliveryErrorTearOff();

mixin _$DeliveryError<T> {
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required
        Result beforeDelivery(
            Exception exception, RequestData<Object> requestData),
    @required Result afterDelivery(DeliveryResponse<T> deliveryResponse),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result beforeDelivery(Exception exception, RequestData<Object> requestData),
    Result afterDelivery(DeliveryResponse<T> deliveryResponse),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result beforeDelivery(_BeforeDelivery<T> value),
    @required Result afterDelivery(_AfterDelivery<T> value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result beforeDelivery(_BeforeDelivery<T> value),
    Result afterDelivery(_AfterDelivery<T> value),
    @required Result orElse(),
  });
}

abstract class $DeliveryErrorCopyWith<T, $Res> {
  factory $DeliveryErrorCopyWith(
          DeliveryError<T> value, $Res Function(DeliveryError<T>) then) =
      _$DeliveryErrorCopyWithImpl<T, $Res>;
}

class _$DeliveryErrorCopyWithImpl<T, $Res>
    implements $DeliveryErrorCopyWith<T, $Res> {
  _$DeliveryErrorCopyWithImpl(this._value, this._then);

  final DeliveryError<T> _value;
  // ignore: unused_field
  final $Res Function(DeliveryError<T>) _then;
}

abstract class _$BeforeDeliveryCopyWith<T, $Res> {
  factory _$BeforeDeliveryCopyWith(
          _BeforeDelivery<T> value, $Res Function(_BeforeDelivery<T>) then) =
      __$BeforeDeliveryCopyWithImpl<T, $Res>;
  $Res call({Exception exception, RequestData<Object> requestData});

  $RequestDataCopyWith<Object, $Res> get requestData;
}

class __$BeforeDeliveryCopyWithImpl<T, $Res>
    extends _$DeliveryErrorCopyWithImpl<T, $Res>
    implements _$BeforeDeliveryCopyWith<T, $Res> {
  __$BeforeDeliveryCopyWithImpl(
      _BeforeDelivery<T> _value, $Res Function(_BeforeDelivery<T>) _then)
      : super(_value, (v) => _then(v as _BeforeDelivery<T>));

  @override
  _BeforeDelivery<T> get _value => super._value as _BeforeDelivery<T>;

  @override
  $Res call({
    Object exception = freezed,
    Object requestData = freezed,
  }) {
    return _then(_BeforeDelivery<T>(
      exception == freezed ? _value.exception : exception as Exception,
      requestData == freezed
          ? _value.requestData
          : requestData as RequestData<Object>,
    ));
  }

  @override
  $RequestDataCopyWith<Object, $Res> get requestData {
    if (_value.requestData == null) {
      return null;
    }
    return $RequestDataCopyWith<Object, $Res>(_value.requestData, (value) {
      return _then(_value.copyWith(requestData: value));
    });
  }
}

class _$_BeforeDelivery<T> implements _BeforeDelivery<T> {
  const _$_BeforeDelivery(this.exception, this.requestData)
      : assert(exception != null),
        assert(requestData != null);

  @override
  final Exception exception;
  @override
  final RequestData<Object> requestData;

  @override
  String toString() {
    return 'DeliveryError<$T>.beforeDelivery(exception: $exception, requestData: $requestData)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _BeforeDelivery<T> &&
            (identical(other.exception, exception) ||
                const DeepCollectionEquality()
                    .equals(other.exception, exception)) &&
            (identical(other.requestData, requestData) ||
                const DeepCollectionEquality()
                    .equals(other.requestData, requestData)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(exception) ^
      const DeepCollectionEquality().hash(requestData);

  @override
  _$BeforeDeliveryCopyWith<T, _BeforeDelivery<T>> get copyWith =>
      __$BeforeDeliveryCopyWithImpl<T, _BeforeDelivery<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required
        Result beforeDelivery(
            Exception exception, RequestData<Object> requestData),
    @required Result afterDelivery(DeliveryResponse<T> deliveryResponse),
  }) {
    assert(beforeDelivery != null);
    assert(afterDelivery != null);
    return beforeDelivery(exception, requestData);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result beforeDelivery(Exception exception, RequestData<Object> requestData),
    Result afterDelivery(DeliveryResponse<T> deliveryResponse),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (beforeDelivery != null) {
      return beforeDelivery(exception, requestData);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result beforeDelivery(_BeforeDelivery<T> value),
    @required Result afterDelivery(_AfterDelivery<T> value),
  }) {
    assert(beforeDelivery != null);
    assert(afterDelivery != null);
    return beforeDelivery(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result beforeDelivery(_BeforeDelivery<T> value),
    Result afterDelivery(_AfterDelivery<T> value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (beforeDelivery != null) {
      return beforeDelivery(this);
    }
    return orElse();
  }
}

abstract class _BeforeDelivery<T> implements DeliveryError<T> {
  const factory _BeforeDelivery(
          Exception exception, RequestData<Object> requestData) =
      _$_BeforeDelivery<T>;

  Exception get exception;
  RequestData<Object> get requestData;
  _$BeforeDeliveryCopyWith<T, _BeforeDelivery<T>> get copyWith;
}

abstract class _$AfterDeliveryCopyWith<T, $Res> {
  factory _$AfterDeliveryCopyWith(
          _AfterDelivery<T> value, $Res Function(_AfterDelivery<T>) then) =
      __$AfterDeliveryCopyWithImpl<T, $Res>;
  $Res call({DeliveryResponse<T> deliveryResponse});

  $DeliveryResponseCopyWith<T, $Res> get deliveryResponse;
}

class __$AfterDeliveryCopyWithImpl<T, $Res>
    extends _$DeliveryErrorCopyWithImpl<T, $Res>
    implements _$AfterDeliveryCopyWith<T, $Res> {
  __$AfterDeliveryCopyWithImpl(
      _AfterDelivery<T> _value, $Res Function(_AfterDelivery<T>) _then)
      : super(_value, (v) => _then(v as _AfterDelivery<T>));

  @override
  _AfterDelivery<T> get _value => super._value as _AfterDelivery<T>;

  @override
  $Res call({
    Object deliveryResponse = freezed,
  }) {
    return _then(_AfterDelivery<T>(
      deliveryResponse == freezed
          ? _value.deliveryResponse
          : deliveryResponse as DeliveryResponse<T>,
    ));
  }

  @override
  $DeliveryResponseCopyWith<T, $Res> get deliveryResponse {
    if (_value.deliveryResponse == null) {
      return null;
    }
    return $DeliveryResponseCopyWith<T, $Res>(_value.deliveryResponse, (value) {
      return _then(_value.copyWith(deliveryResponse: value));
    });
  }
}

class _$_AfterDelivery<T> implements _AfterDelivery<T> {
  const _$_AfterDelivery(this.deliveryResponse)
      : assert(deliveryResponse != null);

  @override
  final DeliveryResponse<T> deliveryResponse;

  @override
  String toString() {
    return 'DeliveryError<$T>.afterDelivery(deliveryResponse: $deliveryResponse)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _AfterDelivery<T> &&
            (identical(other.deliveryResponse, deliveryResponse) ||
                const DeepCollectionEquality()
                    .equals(other.deliveryResponse, deliveryResponse)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(deliveryResponse);

  @override
  _$AfterDeliveryCopyWith<T, _AfterDelivery<T>> get copyWith =>
      __$AfterDeliveryCopyWithImpl<T, _AfterDelivery<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required
        Result beforeDelivery(
            Exception exception, RequestData<Object> requestData),
    @required Result afterDelivery(DeliveryResponse<T> deliveryResponse),
  }) {
    assert(beforeDelivery != null);
    assert(afterDelivery != null);
    return afterDelivery(deliveryResponse);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result beforeDelivery(Exception exception, RequestData<Object> requestData),
    Result afterDelivery(DeliveryResponse<T> deliveryResponse),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (afterDelivery != null) {
      return afterDelivery(deliveryResponse);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result beforeDelivery(_BeforeDelivery<T> value),
    @required Result afterDelivery(_AfterDelivery<T> value),
  }) {
    assert(beforeDelivery != null);
    assert(afterDelivery != null);
    return afterDelivery(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result beforeDelivery(_BeforeDelivery<T> value),
    Result afterDelivery(_AfterDelivery<T> value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (afterDelivery != null) {
      return afterDelivery(this);
    }
    return orElse();
  }
}

abstract class _AfterDelivery<T> implements DeliveryError<T> {
  const factory _AfterDelivery(DeliveryResponse<T> deliveryResponse) =
      _$_AfterDelivery<T>;

  DeliveryResponse<T> get deliveryResponse;
  _$AfterDeliveryCopyWith<T, _AfterDelivery<T>> get copyWith;
}
