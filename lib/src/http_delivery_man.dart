import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';

import './delivery_man.dart';
import './delivery_response.dart';
import './either_http_delivery_man_response_parser.dart';
import './package.dart';
import './post_office_base.dart';

class HttpDeliveryMan<T, R> implements DeliveryMan<T, R> {
  @override
  final Package<T> package;
  @override
  final PostOfficeConfig config;
  final http.BaseClient client;
  final HttpDeliveryManResponseParser<R> responseParser;

  HttpDeliveryMan({
    @required this.config,
    @required this.package,
    HttpDeliveryManResponseParser<R> responseParser,
    http.Client client,
  })  : client = client ?? http.Client(),
        responseParser =
            responseParser ?? EitherHttpDeliveryManStringResponseParser();

  @override
  Future<R> deliver() async {
    final url = _buildUrl();
    final headers = <String, String>{};
    headers.addAll(config.headers);
    headers.addAll(package.headers);
    /*Snapshot of original RequestData to prevent any
    modification before calling responseParser*/
    // ---------------------------------------------------------------------/**/
    final innerConfig = PostOfficeConfig(
        cityAddress: config.cityAddress, headers: config.headers);
    final requestData = RequestData(package, innerConfig);
    // ---------------------------------------------------------------------/**/

    try {
      return await package.map(
        delete: (pack) =>
            parseResponse(client.delete(url, headers: headers), requestData),
        get: (pack) =>
            parseResponse(client.get(url, headers: headers), requestData),
        patch: (pack) => parseResponse(
            client.patch(
              url,
              headers: headers,
              body: pack.body,
            ),
            requestData),
        post: (pack) => parseResponse(
            client.post(
              url,
              headers: headers,
              body: pack.body,
            ),
            requestData),
        put: (pack) => parseResponse(
            client.put(
              url,
              headers: headers,
              body: pack.body,
            ),
            requestData),
      );
    } catch (ex) {
      return responseParser.handleException(ex, requestData);
    }
  }

  String _buildUrl() {
    var builtUrl = '${config.cityAddress}${package.relativeAddress}';
    package.addressParams.forEach(
      (key, value) {
        builtUrl = builtUrl.replaceAll('{$key}', '$value');
      },
    );

    if (package.addressQueries.isNotEmpty) {
      builtUrl = '$builtUrl?';
      package.addressQueries.forEach((key, value) {
        builtUrl = '$builtUrl$key=$value&';
      });

      builtUrl = builtUrl.substring(0, builtUrl.length - 1);
    }

    return builtUrl;
  }

  Future<R> parseResponse(
      Future<http.Response> request, RequestData<T> requestData) async {
    final response = await request;
    return responseParser.parseResponse(response, requestData);
  }
}

abstract class HttpDeliveryManResponseParser<R> {
  R parseResponse(http.Response response, RequestData<Object> requestData);

  R handleException(Exception exception, RequestData<Object> requestData);
}
