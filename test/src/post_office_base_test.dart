import 'package:mockito/mockito.dart';
import 'package:post_office/post_office.dart';
import 'package:post_office/src/delivery_man.dart';
import 'package:post_office/src/http_delivery_man.dart';
import 'package:post_office/src/package.dart';
import 'package:test/test.dart';

class DummyPostOffice extends PostOffice<String> {
  DummyPostOffice(
      {Map<String, String> defaultHeaders = const {},
      DeliveryMan<T, String> Function<T>(
              {Package<T> package, PostOfficeConfig config})
          deliveryManFactory})
      : super(
            cityAddress: 'https://metadata.com/',
            defaultHeaders: defaultHeaders,
            deliveryManFactory: deliveryManFactory);
}

class MockDeliveryMan<T, R> extends Mock implements DeliveryMan<T, R> {}

class MockHttpDeliveryManResponseParser extends Mock
    implements HttpDeliveryManResponseParser<String> {}

void main() {
  group('PostOffice', () {
    PostOffice postOffice;

    setUp(() {
      final fn = <T>({Package<T> package, PostOfficeConfig config}) =>
          HttpDeliveryMan<T, String>(
              config: config,
              package: Package.NULL_PACKAGE,
              responseParser: MockHttpDeliveryManResponseParser());

      postOffice = DummyPostOffice(
          deliveryManFactory: fn,
          defaultHeaders: {'defaultHeaderKey': 'defaultHeaderValue'});
    });

    test('should not allow defaultHeaders direct modification', () {
      expect(() => postOffice.defaultHeaders['newKey'] = 'newValue',
          throwsUnsupportedError);
    });

    test('should add header to defaultHeaders', () {
      final newKey = 'newKey';
      final initialLength = postOffice.defaultHeaders.length;

      expect(postOffice.defaultHeaders, isNot(containsValue(newKey)));

      final newValue = 'newValue';
      postOffice.addHeader(newKey, newValue);
      final finalLength = postOffice.defaultHeaders.length;

      expect(finalLength, equals(initialLength + 1));
      expect(postOffice.defaultHeaders, containsPair(newKey, newValue));
    });

    test(
        'should return a new DeliveryMan<T> instance when a package is received',
        () {
      final firstDeliveryMan = postOffice.receive(Package.NULL_PACKAGE);
      final secondDeliveryMan = postOffice.receive(Package.NULL_PACKAGE);

      expect(firstDeliveryMan, isA<DeliveryMan<Object, Object>>());
      expect(firstDeliveryMan, isNot(same(secondDeliveryMan)));
    });

    test(
        'should use default config to build a deliveryMan if no config is passed',
        () {
      final deliveryMan = postOffice.receive(Package.NULL_PACKAGE);

      expect(
          deliveryMan.config,
          PostOfficeConfig(
              cityAddress: postOffice.cityAddress,
              headers: postOffice.defaultHeaders));
    });

    test('should use passed config instead of default to build a deliveryMan',
        () {
      final config = PostOfficeConfig(
          cityAddress: 'http://anotherCityAddress/',
          headers: {'anotherHeader': 'anotherValue'});
      final deliveryMan =
          postOffice.receive(Package.NULL_PACKAGE, config: config);

      expect(deliveryMan.config, config);
    });

    test('should create a DeliveryMan and deliver the received package',
        () async {
      final deliveryMan = MockDeliveryMan<dynamic, String>();
      final innerPostOffice = PostOffice<String>(
        cityAddress: 'http://anotherCityAddress',
        defaultHeaders: {'anotherDefaultHeader': 'anotherDefaultValue'},
        deliveryManFactory: <Unit>(
                {PostOfficeConfig config, Package<Unit> package}) =>
            (deliveryMan as DeliveryMan<Unit, String>),
      );

      await innerPostOffice.deliver(Package.NULL_PACKAGE);
      verify(deliveryMan.deliver()).called(1);
    });
  });
}
