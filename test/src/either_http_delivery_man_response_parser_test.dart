import 'package:dartz/dartz.dart';
import 'package:http/http.dart';
import 'package:post_office/post_office.dart';
import 'package:post_office/src/delivery_man.dart';
import 'package:post_office/src/delivery_response.dart';
import 'package:post_office/src/either_http_delivery_man_response_parser.dart';
import 'package:post_office/src/http_delivery_man.dart';
import 'package:post_office/src/package.dart';
import 'package:test/test.dart';

class DummyPostOffice
    extends PostOffice<Either<DeliveryError, DeliveryResponse>> {
  DummyPostOffice(
      {Map<String, String> defaultHeaders = const {},
      DeliveryMan<T, Either<DeliveryError, DeliveryResponse>> Function<T>(
              {Package<T> package, PostOfficeConfig config})
          deliveryManFactory})
      : super(
            cityAddress: 'https://metadata.com/',
            defaultHeaders: defaultHeaders,
            deliveryManFactory: deliveryManFactory);
}

void main() {
  DummyPostOffice postOffice;

  group(
    'DoubleEitherHttpResponseParser',
    () {
      setUp(() {
        final fn = <T>({Package<T> package, PostOfficeConfig config}) =>
            HttpDeliveryMan<T, Either<DeliveryError, DeliveryResponse>>(
                config: config,
                package: Package.NULL_PACKAGE,
                responseParser: EitherHttpDeliveryManStringResponseParser());

        postOffice = DummyPostOffice(
          deliveryManFactory: fn,
        );
      });

      test('should convert http.Response success to Right', () {
        final eitherResponse = EitherHttpDeliveryManStringResponseParser();

        final response = Response('Successful response', 200,
            headers: {'aHeader': 'aHeaderValue'},
            reasonPhrase: 'OK',
            isRedirect: false,
            persistentConnection: false);

        final config = PostOfficeConfig(
            cityAddress: postOffice.cityAddress,
            headers: postOffice.defaultHeaders);

        final requestData = RequestData(Package.NULL_PACKAGE, config);
        final expectedResponse = Right<DeliveryError, DeliveryResponse<String>>(
            DeliveryResponse(200, requestData,
                reasonPhrase: 'OK',
                headers: {'aHeader': 'aHeaderValue'},
                body: 'Successful response',
                bodyBytes: response.bodyBytes,
                contentLength: response.contentLength));

        final deliveryMan = postOffice.receive(Package.NULL_PACKAGE);
        deliveryMan.deliver();

        final parsedResponse =
            eitherResponse.parseResponse(response, requestData);

        expect(parsedResponse, equals(expectedResponse));
      });

      test('should convert http.Response error to Left afterDelivery error',
          () {
        final eitherResponse = EitherHttpDeliveryManStringResponseParser();

        final response = Response('Not found', 404,
            headers: {'aHeader': 'aHeaderValue'},
            reasonPhrase: 'Not found',
            isRedirect: false,
            persistentConnection: false);

        final config = PostOfficeConfig(
            cityAddress: postOffice.cityAddress,
            headers: postOffice.defaultHeaders);

        final requestData = RequestData(Package.NULL_PACKAGE, config);

        final deliveryResponse = DeliveryResponse(404, requestData,
            reasonPhrase: 'Not found',
            headers: {'aHeader': 'aHeaderValue'},
            body: 'Not found',
            bodyBytes: response.bodyBytes,
            contentLength: response.contentLength);
        final afterDeliveryError =
            DeliveryError.afterDelivery(deliveryResponse);
        final expectedResponse = Left(afterDeliveryError);

        final deliveryMan = postOffice.receive(Package.NULL_PACKAGE);
        deliveryMan.deliver();

        final parsedResponse =
            eitherResponse.parseResponse(response, requestData);

        expect(parsedResponse, equals(expectedResponse));
      });

      test(
          'should convert http.Response exception to Left beforeDelivery error',
          () {
        final eitherResponse = EitherHttpDeliveryManStringResponseParser();

        final exception = Exception('an unexpected exception');

        final config = PostOfficeConfig(
            cityAddress: postOffice.cityAddress,
            headers: postOffice.defaultHeaders);

        final requestData = RequestData(Package.NULL_PACKAGE, config);

        final beforeDeliveryError =
            DeliveryError<String>.beforeDelivery(exception, requestData);
        final expectedResponse = Left(beforeDeliveryError);

        final deliveryMan = postOffice.receive(Package.NULL_PACKAGE);
        deliveryMan.deliver();

        final parsedResponse =
            eitherResponse.handleException(exception, requestData);

        expect(parsedResponse, equals(expectedResponse));
      });
    },
  );
}
