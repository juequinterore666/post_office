import 'package:http/http.dart';
import 'package:mockito/mockito.dart';
import 'package:post_office/post_office.dart';
import 'package:post_office/src/delivery_man.dart';
import 'package:post_office/src/delivery_response.dart';
import 'package:post_office/src/http_delivery_man.dart';
import 'package:post_office/src/package.dart';
import 'package:test/test.dart';

class MockHttpClient extends Mock implements BaseClient {}

class DummyPostOffice extends PostOffice<DeliveryResponse> {
  DummyPostOffice(
      {Map<String, String> defaultHeaders = const {},
      DeliveryMan<T, DeliveryResponse> Function<T>(
              {Package<T> package, PostOfficeConfig config})
          deliveryManFactory})
      : super(
            cityAddress: 'https://metadata.com/',
            defaultHeaders: defaultHeaders,
            deliveryManFactory: deliveryManFactory);
}

class MockResponseParser extends Mock
    implements HttpDeliveryManResponseParser<DeliveryResponse> {}

void main() {
  group(
    'HttpDeliveryMan',
    () {
      MockHttpClient mockHttpClient;
      PostOffice<DeliveryResponse> postOffice;
      HttpDeliveryManResponseParser<DeliveryResponse> mockResponseParser;

      setUp(() {
        mockHttpClient = MockHttpClient();
        mockResponseParser = MockResponseParser();
        postOffice = DummyPostOffice(
            defaultHeaders: {'defaultHeader1': 'value1'},
            deliveryManFactory: <T>(
                    {Package<T> package, PostOfficeConfig config}) =>
                HttpDeliveryMan<T, DeliveryResponse>(
                    client: mockHttpClient,
                    package: package,
                    config: config,
                    responseParser: mockResponseParser));
      });

      group(
        'requests',
        () {
          test(
            'should call http.client delete with correct values if using a .DELETE package',
            () {
              final deletePackage = Package.delete(
                  'deleteTest/{param1}/{param2}',
                  addressParams: {'param1': '1', 'param2': 'value2'},
                  addressQueries: {'query1': '1', 'query2': 'value2'},
                  headers: {'headers1': '1', 'headers2': 'value2'});

              final deliveryMan = postOffice.receive(deletePackage);
              deliveryMan.deliver();

              verify(
                mockHttpClient.delete(
                  'https://metadata.com/deleteTest/1/value2?query1=1&query2=value2',
                  headers: {
                    'defaultHeader1': 'value1',
                    'headers1': '1',
                    'headers2': 'value2'
                  },
                ),
              );
            },
          );

          test(
            'should call http.client get with correct values if using a .GET package',
            () {
              final getPackage = Package.get('getTest/{param1}/{param2}',
                  addressParams: {'param1': '1', 'param2': 'value2'},
                  addressQueries: {'query1': '1', 'query2': 'value2'},
                  headers: {'headers1': '1', 'headers2': 'value2'});

              final deliveryMan = postOffice.receive(getPackage);
              deliveryMan.deliver();

              verify(
                mockHttpClient.get(
                  'https://metadata.com/getTest/1/value2?query1=1&query2=value2',
                  headers: {
                    'defaultHeader1': 'value1',
                    'headers1': '1',
                    'headers2': 'value2'
                  },
                ),
              );
            },
          );

          test(
            'should call http.client get with correct values if using a .GET package without queries',
            () {
              final getPackage = Package.get('getTest/{param1}/{param2}',
                  addressParams: {'param1': '1', 'param2': 'value2'},
                  headers: {'headers1': '1', 'headers2': 'value2'});

              final deliveryMan = postOffice.receive(getPackage);
              deliveryMan.deliver();

              verify(
                mockHttpClient.get(
                  'https://metadata.com/getTest/1/value2',
                  headers: {
                    'defaultHeader1': 'value1',
                    'headers1': '1',
                    'headers2': 'value2'
                  },
                ),
              );
            },
          );

          test(
            'should call http.client patch with correct values if using a .PATCH package',
            () {
              final patchPackage = Package.patch('patchTest/{param1}/{param2}',
                  addressParams: {'param1': '1', 'param2': 'value2'},
                  addressQueries: {'query1': '1', 'query2': 'value2'},
                  body: 'A simple string',
                  headers: {'headers1': '1', 'headers2': 'value2'});

              final deliveryMan = postOffice.receive(patchPackage);
              deliveryMan.deliver();

              verify(
                mockHttpClient.patch(
                    'https://metadata.com/patchTest/1/value2?query1=1&query2=value2',
                    headers: {
                      'defaultHeader1': 'value1',
                      'headers1': '1',
                      'headers2': 'value2'
                    },
                    body: 'A simple string'),
              );
            },
          );

          test(
            'should call http.client post with correct values if using a .POST package',
            () {
              final postPackage = Package.post('postTest/{param1}/{param2}',
                  addressParams: {'param1': '1', 'param2': 'value2'},
                  addressQueries: {'query1': '1', 'query2': 'value2'},
                  body: 'A simple string',
                  headers: {'headers1': '1', 'headers2': 'value2'});

              final deliveryMan = postOffice.receive(postPackage);
              deliveryMan.deliver();

              verify(
                mockHttpClient.post(
                    'https://metadata.com/postTest/1/value2?query1=1&query2=value2',
                    headers: {
                      'defaultHeader1': 'value1',
                      'headers1': '1',
                      'headers2': 'value2'
                    },
                    body: 'A simple string'),
              );
            },
          );

          test(
            'should call http.client put with correct values if using a .PUT package',
            () {
              final putPackage = Package.put('putTest/{param1}/{param2}',
                  addressParams: {'param1': '1', 'param2': 'value2'},
                  addressQueries: {'query1': '1', 'query2': 'value2'},
                  body: 'A simple string',
                  headers: {'headers1': '1', 'headers2': 'value2'});

              final deliveryMan = postOffice.receive(putPackage);
              deliveryMan.deliver();

              verify(
                mockHttpClient.put(
                    'https://metadata.com/putTest/1/value2?query1=1&query2=value2',
                    headers: {
                      'defaultHeader1': 'value1',
                      'headers1': '1',
                      'headers2': 'value2'
                    },
                    body: 'A simple string'),
              );
            },
          );
        },
      );

      group(
        'responses',
        () {
          test(
            'should call responseParser parseResponse with http.Response',
            () async {
              final getPackage = Package.NULL_PACKAGE;

              final response =
                  Response('Successful response', 200, reasonPhrase: 'OK');

              when(mockHttpClient.get(any, headers: anyNamed('headers')))
                  .thenAnswer((_) => Future.value(response));

              final deliveryMan = postOffice.receive(getPackage);
              await deliveryMan.deliver();
              verify(mockResponseParser.parseResponse(response, any));
            },
          );

          test(
            'should call responseParser parseResponse with original requestData (not modified after request sent but before response)',
            () {
              final getPackage = Package.NULL_PACKAGE;
              final config = PostOfficeConfig(
                  cityAddress: postOffice.cityAddress,
                  headers: postOffice.defaultHeaders);
              final requestData = RequestData(getPackage, config);

              final response =
                  Response('Successful response', 200, reasonPhrase: 'OK');

              when(mockHttpClient.get(any, headers: anyNamed('headers')))
                  .thenAnswer((_) => Future.value(response));

              final deliveryMan = postOffice.receive(getPackage);
              // Not using await to simulate postOffice defaultHeaders change during request process
              deliveryMan.deliver().then((a) => verify(
                  mockResponseParser.parseResponse(response, requestData)));
              postOffice.addHeader('newHeader', 'newHeaderValue');
            },
          );

          test(
            'should call responseParser parseResponse with original requestData (not modified after request sent but before response)',
            () {
              final getPackage = Package.NULL_PACKAGE;
              final config = PostOfficeConfig(
                  cityAddress: postOffice.cityAddress,
                  headers: postOffice.defaultHeaders);
              final requestData = RequestData(getPackage, config);

              final response =
                  Response('Successful response', 200, reasonPhrase: 'OK');

              when(mockHttpClient.get(any, headers: anyNamed('headers')))
                  .thenAnswer((_) => Future.value(response));

              final deliveryMan = postOffice.receive(getPackage);
              // Not using await to simulate postOffice defaultHeaders change during request process
              deliveryMan.deliver().then((a) => verify(
                  mockResponseParser.parseResponse(response, requestData)));
              postOffice.addHeader('newHeader', 'newHeaderValue');
            },
          );

          test(
            'should call responseParser sendError with error thrown from Future if something went wrong during delivery',
            () async {
              final getPackage = Package.NULL_PACKAGE;

              final exception = Exception('an unexpected error');

              when(mockHttpClient.get(any, headers: anyNamed('headers')))
                  .thenAnswer((_) => Future<Response>.error(exception));

              final deliveryMan = postOffice.receive(getPackage);
              await deliveryMan.deliver();
              verify(mockResponseParser.handleException(exception, any));
            },
          );

          test(
            'should call responseParser sendError with error if something went wrong during delivery',
            () async {
              final getPackage = Package.NULL_PACKAGE;

              final exception = Exception('an unexpected error');

              when(mockHttpClient.get(any, headers: anyNamed('headers')))
                  .thenThrow(exception);

              final deliveryMan = postOffice.receive(getPackage);
              await deliveryMan.deliver();
              verify(mockResponseParser.handleException(exception, any));
            },
          );
        },
      );
    },
  );
}
